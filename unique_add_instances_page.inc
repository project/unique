<?php

/**
 * @file
 * Part of the unique product module: a page which allows easily adding
 * numerous unique product instances to the store.
 */


/**
 * Menu callback: renders the Add Instances page
 */
function unique_add_instances_page() {
  $form = unique_add_instances_form();
  return drupal_get_form('unique_add_instances_page', $form);
}


/**
 * Builds and returns the Add Instances page form.
 *
 * Rendering form elements within a table in the form code is adapted from code
 * by Ryan Szrama (http://ryan.grinhost.net).
 */
function unique_add_instances_form() {
  $options = get_all_unique_products_options();
  if (count($options) == 0) {
    form_set_error('', t('You must create at least one product class before creating instances.'));
    return;
  }

  $form['class'] = array(
    '#type' => 'select',
    '#title' => t("Product"),
    '#options' => $options,
  );

  $form['new'] = array('#tree' => TRUE);
  for ($i = 0; $i < NUM_ADD_INSTANCES; ++$i) {
    $form['new'][$i]['title'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 50
    );
    $form['new'][$i]['body'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#maxlength' => 100
    );
    $form['new'][$i]['internal_notes'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#maxlength' => 100
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add new instance(s)')
  );
  return $form;
}


/**
 * Default theme function for the add instances page.
 */
function theme_unique_add_instances_page($form) {
  foreach(element_children($form['new']) as $i) {
    $rows[] = array(
      form_render($form['new'][$i]['title']),
      form_render($form['new'][$i]['body']),
      form_render($form['new'][$i]['internal_notes'])
    );
  }

  $header = array('title', 'short description', 'internal notes');
  $output = theme_table($header, $rows);
  $output .= form_render($form);
  return $output;
}


/**
 * Implementation of form_submit().
 */
function unique_add_instances_page_submit($form_id, $form_values) {
  global $user;

  $pid = $form_values['class'];
  for ($i = 0; $i < NUM_ADD_INSTANCES; ++$i) {
    if (!$form_values['new'][$i]['title'])
      continue;
    $new_node->uid      = $user->uid;
    $new_node->type     = 'product';
    $new_node->ptype    = 'unique_instance';
    $new_node->pid      = $pid;
    $new_node->title    = $form_values['new'][$i]['title'];
    $new_node->body     = $form_values['new'][$i]['body'];
    $new_node->internal_notes = $form_values['new'][$i]['internal_notes'];
    $new_node->teaser   = node_teaser($new_node->body);
    $new_node->filter   = variable_get('filter_default_format', 1);
    $new_node->status   = 0;
    $new_node->revision = 0;
    $new_node->promote  = 0;
    $new_node->sticky   = 0;
    $new_node->comment  = 2;
    node_save($new_node);
    unset($new_node);
  }
  drupal_set_message(t('Created new instances.'));
}

?>
