Product Module  : UNIQUE
Original Author : Adam DiCarlo <adam@f2it.com>
Settings        : 

********************************************************************
DESCRIPTION:

Creates a "unique" product type - a class of products which can be
instantiated with unique product instances (unique_instance module).

This module along with unique_instance allows customers to place
items in their cart which are specific instances of a unique product
type. Items in cart are "on-hold" - the specific unique_instance
in cart cannot be added to someone else's cart. It won't appear
on instance-selection, either.

The product class allows a plain-texty "time limit" for having the
item on-hold in cart. A cron script throws items out of the cart
if they've expired, but you must have cron running often enough
for this to happen, if the time limit is low, for instance.

Due to the on-hold nature, anonymous users are not allowed to add
items to cart or even see the instance selection.

NOTE:
Tested with drupal-4.7.6 + ecommerce 3 beta 1.

There is no localization.

postgresql is not currently supported, as I don't have a postgresql
drupal instance set up. If you want postgres support, let me know.
********************************************************************


See MAINTAINERS.txt for more maintenance info.
See README.txt (in E-Commerce root) for other info.









